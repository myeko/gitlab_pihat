# GitLab_PiHAt
This is a UPS (uninterrupted power supply) HAT.
This microHAT will keep your Pi alive for long enough to shut it
down properly (or if your battery is big enough to survive the blackout).
